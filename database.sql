CREATE DATABASE IF NOT EXISTS `paste`;
USE `paste`;

CREATE TABLE IF NOT EXISTS `language` (
  `Name` varchar(50) NOT NULL,
  `Label` varchar(100) DEFAULT NULL,
  `Priority` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `paste` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Slug` varchar(50) NOT NULL,
  `IP` varchar(50) NOT NULL,
  `Posted` datetime NOT NULL,
  `Expires` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Paste` mediumtext,
  `DeleteLink` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Slug` (`Slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;