<?php
class PasteModel extends Model {
    public function PasteModel() {
        parent::__construct();
    }

    public function getPaste($slug) {
        $req = 'SELECT `Slug`, `IP`, `Posted`, `Expires`, `Code`, `Paste`, `DeleteLink`
                FROM `paste`
                WHERE `Slug` = ?';

        $st = $this->db->prepare($req);
        $st->execute(array($slug));
        $rs = $st->fetch();

        return $rs;
    }

    public function addPaste($data, $code = null) {
        $req = 'INSERT INTO `paste` (`Slug`, `IP`, `Posted`, `Code`, `Paste`, `DeleteLink`) VALUES (?, ?, ?, ?, ?, ?)';
        $slug = '';

        $st = $this->db->prepare($req);

        while(true) {
            try {
                $slug = $this->genSlug();

                $d = array(
                    $slug,
                    $_SERVER['REMOTE_ADDR'],
                    Helpers::formatSQLDate(time()),
                    $code,
                    $data['paste'],
                    ''
                );

                $st->execute($d);
                break;
            } catch (PDOException $e) {
                if ($e->errorInfo[1] != 1062) {
                    return false;
                }
            }
        }

        return $slug;
    }

    private function genSlug() {
        $slug = '';
        $consonants = 'bcdfghjklmnprstvwz';
        $vowels = 'aeiou';
        $all = $consonants.$vowels; 
         
        for($i = 0; $i < 2; $i++){
            $slug .= $consonants[rand(0, strlen($consonants)-1)];
            $slug .= $vowels[rand(0, strlen($vowels)-1)];
            $slug .= $all[rand(0, strlen($all)-1)];
        }

        return $slug;
    }
}