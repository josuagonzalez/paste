<?php
class LanguageModel extends Model {
    public function LanguageModel() {
        parent::__construct();
    }

    public function getLanguages() {
        $req = 'SELECT `Name`, `Label` FROM `language` ORDER BY `Priority` DESC, `Label` ASC';
        $st = $this->db->prepare($req);
        $st->execute();
        
        return $st->fetchAll();
    }
}