<?php
class Paste extends Controller {
    public function Paste() {
        parent::__construct();
    }

    public function index($args) {
        if(empty($args['p'])) 
            Helpers::redirect('', 'add');

        $pastemodel = new PasteModel();
        $paste = $pastemodel->getPaste($args['p']);
        if($paste === false) $this->notifyError('This paste doesn\'t exists, was deleted or has expired');

        $paste['link'] = Helpers::makeUrl('', null, array('p' => $args['p']));
        $paste['directLink'] = Helpers::makeUrl('', 'direct', array('p' => $args['p']));

        $this->afk->view('pasteview', array('paste' => $paste));
    }

    public function direct($args) {
        $pastemodel = new PasteModel();
        $paste = $pastemodel->getPaste($args['p']);
        if($paste === false) $this->notifyError('This paste doesn\'t exists, was deleted or has expired');

        header("Content-Type:text/plain; charset=utf-8");
        echo $paste['Paste'];
        exit();
    }

    public function add() {
        $lngModel = new LanguageModel();
        $langs = $lngModel->getLanguages();
        $selectValues = array('none' => 'Normal text');

        foreach ($langs as $lang) {
            $selectValues[$lang['Name']] = $lang['Label'];
        }

        $form = new Form();
        $form->addField('paste', 'textarea', array('label' => 'Paste here :', 'class' => 'paste'));
        $form->addField('code', 'select', array('values' => $selectValues));
        $form->addSubmit('Paste it !');

        $this->afk->view('pasteadd', array('form' => $form->generate(Helpers::makeUrl('', 'post'))));
    }

    public function post() {
        if(empty($_POST['paste'])) 
            $this->notifyError('Missing data');

        $lngModel = new LanguageModel();
        $langs = $lngModel->getLanguages();
        $supported = array();
        foreach ($langs as $lang) {
            $supported[] = $lang['Name'];
        }

        $code = null;
        if(isset($_POST['code']) && $_POST['code'] != 'none')
            if(in_array($_POST['code'], $supported))
                $code = $_POST['code'];

        $pastemodel = new PasteModel();
        $pasteid = $pastemodel->addPaste($_POST, $code);
        Helpers::redirect('', '', array('p' => $pasteid));
    }

    private function notifyError($error) {
        Helpers::notify('Error', $error, 'error');
        Helpers::redirect('');
    }
}