<?php
class Form {
    private $fields = array();

    public function addField($name, $type, $params = null) {
        $field = array('name' => $name, 'type' => $type);
        if(empty($params)) $params = array();
        if(isset($name)) $field['name'] = $name;
        if(isset($params['label'])) $field['label'] = $params['label'];
        if(isset($params['hint'])) $field['hint'] = $params['hint'];
        if(isset($params['value'])) $field['value'] = $params['value'];
        if(isset($params['values'])) $field['values'] = $params['values'];
        if(isset($params['checked'])) $field['checked'] = $params['checked'];
        if(isset($params['class'])) $field['class'] = $params['class'];

        $this->fields[] = $field;
    }

    public function addSubmit($text = null) {
        $this->addField(null, 'submit', array('value' => $text));
    }

    public function startRadioGroup($label, $name = null, $value = null) {
        $this->fields[] = array('type' => 'radiogrp', 'label' => $label, 'name' => $name, 'value' => $value);
    }

    public function endRadioGroup() {
        $this->fields[] = array('type' => 'endradiogrp');
    }

    public function addRadio($groupName, $value, $label, $checked = false) {
        $this->addField($groupName, 'radio', array('value' => $value, 'checked' => $checked, 'label' => $label));
    }

    public function addText($text, $class = '') {
        $this->addField('', 'special_text', array('class' => $class)); 
    }

    private function mkId($type, $name, $value = null) {
        $value = (isset($value)) ? '-'.Helpers::slugify($value) : '';
        return 'f-'.Helpers::slugify($type).'-'.Helpers::slugify($name).$value;
    }

    public function generate($action = null, $method = 'POST', $class = null) {
        $output = ($action != null) ? '<form action="'.$action.'" method="'.$method.'">'.PHP_EOL : '';

        foreach ($this->fields as $field) {
            if(isset($field['name']))
            $id = $this->mkId($field['type'], $field['name']);
            $output .= '<span class="field '.$field['type'].'">';
            switch ($field['type']) {
                case 'startset':
                    $legend = (isset($field['label'])) ? '<legend>'.$field['label'].'</legend>' : '';
                    $output .= '<fieldset>'.$legend;
                    break;

                case 'endset';
                    $output .= '</fieldset>';

                case 'radiogrp':
                    $for = (isset($field['name'])) ? ' for="'.$this->mkId('radio', $field['name'], $field['value']).'"' : '';
                    if(isset($field['label'])) $output .= '<label'.$for.'>'.$field['label'].'</label> ';
                    break;

                case 'radio':
                    $id .= '-'.Helpers::slugify($field['value']);
                    $checked = ($field['checked']) ? 'checked="checked"' : '';
                    $output .= '<input id="'.$id.'" type="'.$field['type'].'" name="'.$field['name'].'" '.$checked.'value="'.$field['value'].'"/>';
                    if(isset($field['label'])) $output .= '<label for="'.$id.'">'.$field['label'].'</label>';
                    break;

                case 'endradiogrp':
                    $output .= '<br />';
                    break;

                case 'checkbox':
                    $checked = ($field['checked']) ? 'checked="checked"' : '';
                    $placeholder = (isset($field['hint'])) ? 'placeholder="'.$field['hint'].'"' : '';
                    $output .= '<input id="'.$id.'" type="'.$field['type'].'" name="'.$field['name'].'" '.$checked.'/>';
                    if(isset($field['label'])) $output .= '<label for="'.$id.'">'.$field['label'].'</label><br />';
                    break;

                case 'select':
                    if(isset($field['label'])) $output .= '<label for="'.$id.'">'.$field['label'].'</label>';
                    $c = (isset($field['class'])) ? ' class="'.$field['class'].'"' : '';
                    $output .= '<select name="'.$field['name'].'"'.$c.'>'.PHP_EOL;

                    foreach ($field['values'] as $key => $value) {
                        $output .= '    <option value="'.$key.'">'.$value.'</option>'.PHP_EOL;
                    }

                    $output .= '</select>';
                    break;

                case 'submit':
                    if(isset($field['label'])) $output .= '<label for="'.$id.'">'.$field['label'].'</label><br />';
                    $output .= '<input type="submit"'.(isset($field['value']) ? ' value="'.$field['value'].'"' : '').' /><br />';
                    break;

                case 'textarea':
                    if(isset($field['label'])) $output .= '<label for="'.$id.'">'.$field['label'].'</label><br />';
                    $output .= '<textarea id="'.$id.'" name="'.$field['name'].'">'.(isset($field['value']) ? $field['value'] : '').'</textarea><br />';
                    break;
                
                default:
                    if(isset($field['label'])) $output .= '<label for="'.$id.'">'.$field['label'].'</label> ';
                    $output .= '<input id="'.$id.'" name="'.$field['name'].'" /><br />';
                    break;
            }

            $output .= '</span>'.PHP_EOL;
        }

        $output .= ($action != null) ? '</form>' : '';
        return $output;
    }
}