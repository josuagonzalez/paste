<?php
class Helpers {    
    public static function makeUrl($action, $method = null, $args = null, $htmlencode = true) {
        $sep = ($htmlencode) ? '&amp;' : '&';
        $url = Config::$app['baseurl'].'index.php?';
        $url .= 'action='.$action;

        // CUSTOM REWRITE RULES
        if($action == '' && $method == null && isset($args['p'])) 
            return Config::$app['baseurl'].'p'.$args['p'];

        if($action == '' && $method == 'direct' && isset($args['p'])) 
            return Config::$app['baseurl'].'d'.$args['p'];

        if($action == '' && $method == 'add') 
            return Config::$app['baseurl'].'new';

        if($action == '' && $method == 'post') 
            return Config::$app['baseurl'].'post';
        // DONE

        if($method != null) {
            $url .= $sep;
            $url .= 'method='.$method;
        }

        if($args != null) {
            if(is_array($args)) {
                foreach ($args as $key => $value) {
                    $url .= $sep;
                    if(!is_int($key)) {
                        $url .= rawurlencode($key).'=';
                    }
                    
                    $url .= rawurlencode($value);
                }

            } else {
                $url .= $sep.$args;
            }
        }

        return $url;
    }

    public static function notify($title, $message, $type = 'info') {
        $_SESSION['n.title'] = $title;
        $_SESSION['n.message'] = $message;
        $_SESSION['n.type'] = $type;
    }

    public static function unsetNotification() {
        unset($_SESSION['n.title']);
        unset($_SESSION['n.message']);
        unset($_SESSION['n.type']);
    }

    public static function redirect($action, $method = null, $args = null, $message = '') {
        header('Location: '.Helpers::makeUrl($action, $method, $args, false));
        exit($message);
    }
    
    public static function slugify($str) {
        $str = strtr($str, 
          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        return strtolower(preg_replace('/([^.a-z0-9]+)/i', '-', $str));
    }

    public static function formatSQLDate($date) {
        return date('Y-m-d H:i:s', $date);
    }
}
